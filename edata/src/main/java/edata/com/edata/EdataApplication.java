package edata.com.edata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EdataApplication {

	public static void main(String[] args) {
		SpringApplication.run(EdataApplication.class, args);
	}

}
